﻿namespace GrillBot.App.Infrastructure;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class InitializableAttribute : Attribute
{
}
