﻿using GrillBot.Common.Extensions.Discord;

namespace GrillBot.App.Extensions.Discord;

static public class EmbedExtensions
{
    static public EmbedBuilder WithFooter(this EmbedBuilder builder, IUser user)
    {
        return builder.WithFooter(user.GetDisplayName(), user.GetUserAvatarUrl());
    }
}
