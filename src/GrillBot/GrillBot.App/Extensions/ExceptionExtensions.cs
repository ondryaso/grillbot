﻿namespace GrillBot.App.Extensions
{
    static public class ExceptionExtensions
    {
        static public MemoryStream ToMemoryStream(this Exception exception)
        {
            var bytes = Encoding.UTF8.GetBytes(exception.ToString());
            return new MemoryStream(bytes);
        }
    }
}
