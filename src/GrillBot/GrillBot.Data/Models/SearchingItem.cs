﻿namespace GrillBot.Data.Models
{
    public class SearchingItem
    {
        public long Id { get; set; }
        public string DisplayName { get; set; }
        public string Message { get; set; }
        public string JumpLink { get; set; }
    }
}
