﻿namespace GrillBot.Data.Models.API.Common;

public class SortParams
{
    public string OrderBy { get; set; }
    public bool Descending { get; set; }
}
