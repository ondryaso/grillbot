﻿namespace GrillBot.Data.Models.API.Channels;

public class UpdateChannelParams
{
    public long Flags { get; set; }
}
