﻿using GrillBot.Data.Enums;

namespace GrillBot.Data.Models.MessageCache
{
    public class MessageMetadata
    {
        public CachedMessageState State { get; set; }
    }
}
