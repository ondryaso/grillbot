﻿namespace GrillBot.Database.Enums;

public enum SuggestionType
{
    Emote = 0,

    FeatureRequest = 1
}
